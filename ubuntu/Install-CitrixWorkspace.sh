# Prerequisites
sudo apt install gdebi wget -y



echo "\n"
echo "Installation Citrix Workspace App"
echo "****************************************************************************\n\n"
#Install Citrix Rexeiver
#Download Citrix Receiver
PackageName="icaclient_20.10.0.6_amd64.deb"
DownLoadURLPrefix=$(wget -qO- 'https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html' | awk -F 'rel=\"' '/'$PackageName'/ {print $2}'| awk -F'"' '{print $1}'|sed '/^$/d')
 
if [ $(echo $DownLoadURLPrefix | cut -c1-4) != "http" ] ; then
	DownLoadURLPrefix='http:'$DownLoadURLPrefix
fi
 
wget $DownLoadURLPrefix -O $PackageName
 
#Install Package
sudo gdebi $PackageName -n
 
#Add more SSL certificates
sudo ln -s /usr/share/ca-certificates/mozilla/* /opt/Citrix/ICAClient/keystore/cacerts/
sudo c_rehash /opt/Citrix/ICAClient/keystore/cacerts/
 
#Cleanup
rm $PackageName


echo "\n"
echo "Installation Citrix Citrix Workspace App USB Support Package"
echo "****************************************************************************\n\n"
#Download Citrix Receiver USB Support Package
PackageName="ctxusb_20.10.0.6_amd64.deb"
 
DownLoadURLPrefix=$(wget -qO- 'https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html' | awk -F 'rel=\"' '/'$PackageName'/ {print $2}'| awk -F'"' '{print $1}'|sed '/^$/d')
 
if [ $(echo $DownLoadURLPrefix | cut -c1-4) != "http" ] ; then
	DownLoadURLPrefix='http:'$DownLoadURLPrefix
fi
 
wget $DownLoadURLPrefix -O $PackageName
 
#Install Package
sudo gdebi $PackageName -n
 
#Cleanup
rm $PackageName


echo "\n"
echo "Installation Citrix Workspace App HDX RealTime Media Engine for Skype for Business"
echo "****************************************************************************\n\n"
#Download Citrix Receiver HDX RealTime Media Engine for Skype for Business
PackageName="HDX_RealTime_Media_Engine_2.9.200_for_Linux_x64.zip"
DebName="x86_64/citrix-hdx-realtime-media-engine_2.9.200-2506_amd64.deb"
DownLoadURLPrefix=$(wget -qO- 'https://www.citrix.com/downloads/citrix-receiver/additional-client-software/hdx-realtime-media-engine-29200.html' | awk -F 'rel=\"' '/'$PackageName'/ {print $2}'| awk -F'"' '{print $1}'|sed '/^$/d')
 
if [ $(echo $DownLoadURLPrefix | cut -c1-4) != "http" ] ; then
	DownLoadURLPrefix='http:'$DownLoadURLPrefix
fi
 
wget $DownLoadURLPrefix -O $PackageName
 
unzip $PackageName
 
Directory=$(echo $PackageName | awk -F '.zip' '//{print $1}')
mkdir -p /tmp/rtmeok
sudo gdebi "$Directory/$DebName" -n
 
#Cleanup
rm -rf /tmp/rtmeok
rm -r $Directory
rm $PackageName